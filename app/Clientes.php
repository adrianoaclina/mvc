<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $fillable = [
        'id',
        'nome',
        'endereco',
        'email',
        'telefone'
    ];
    protected $table = 'Clientes';

    public function vendas(){
        return $this->hasMany(vendas::class, 'cliente_id');
    }

    public function produtoVenda()
    {
        return $this->hasManyThrough(produtosVenda::class, 'App\Vendas', 'cliente_id', 'id');
    }
    public function vendedores()
    {
        return $this->hasManyThrough(vendedores::class, 'App\Vendas', 'vendedor_id', 'id');
    }
}


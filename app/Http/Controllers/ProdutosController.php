<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produtos;
class ProdutosController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function listar(){
        $produtos = Produtos::all();

        return view('produtos.listar', ['produtos' => $produtos]);
    }
}
